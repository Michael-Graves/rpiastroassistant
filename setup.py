from setuptools import setup, find_packages

requirements = [
    'gphoto2==2.2.2',
    'tornado==6.0.4',
    ]

setup(name="pyastroassistant", version = '0.1', packages=find_packages(), install_requires=requirements)
