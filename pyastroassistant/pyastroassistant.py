import gphoto2 as gp

import os
import tornado.ioloop
import tornado.web
import asyncio
import selectors

class Cam():
    class Abilities():
        # [enum CameraDriverStatus]: Current implementation status of the camera driver.
        GP_DRIVER_STATUS = {
            0x00: "PRODUCTION",         # Driver is production ready.
            0x01: "TESTING",            # Driver is beta quality.
            0x02: "EXPERIMENTAL",       # Driver is alpha quality and might even not work.
            0x03: "DEPRECATED",         # Driver is no longer recommended to use and will be removed.
        }

        # [enum GphotoDeviceType]
        GP_DEVICE = {
            0x00: "STILL_CAMERA",
            0x01: "AUDIO_PLAYER",
        }

        # [enum GPPortType] The gphoto port type. The enum is providing bitmasks, but most code uses it as just the one specific values.
        GP_PORT = {
            0x00: "NONE",               # No specific type associated.
            0x01: "SERIAL",             # Serial port.
            0x04: "USB",                # USB port.
            0x08: "DISK",               # Disk / local mountpoint port.
            0x10: "PTPIP",              # PTP/IP port.
            0x20: "USB_DISK_DIRECT",    # Direct IO to an usb mass storage device.
            0x40: "USB_SCSI",           # USB Mass Storage raw SCSI port.
        }

        # [enum CameraFileOperation]: A bitmask of image related operations of the device.
        GP_FILE_OPERATION = {
            0x00: "NONE",               # No special file operations, just download.
            0x02: "DELETE",             # Deletion of files is possible.
            0x08: "PREVIEW",            # Previewing viewfinder content is possible.
            0x10: "RAW",                # Raw retrieval is possible (used by non-JPEG cameras)
            0x20: "AUDIO",              # Audio retrieval is possible.
            0x40: "EXIF",               # EXIF retrieval is possible.
        }

        # [enum CameraFolderOperation]: A bitmask of filesystem related operations of the device.
        GP_FOLDER_OPERATION = {
            0x00: "NONE",               # No special filesystem operation.
            0x01: "DELETE_ALL",         # Deletion of all files on the device.
            0x02: "PUT_FILE",           # Upload of files to the device possible.
            0x04: "MAKE_DIR",           # Making directories on the device possible.
            0x08: "REMOVE_DIR",         # Removing directories from the device possible.
        }

        # [enum CameraOperation]: A bitmask of remote control related operations of the device. Some drivers might support additional dynamic capabilities (like the PTP driver).
        GP_OPERATION = {
            0x00: "NONE",               # No remote control operation supported.
            0x01: "CAPTURE_IMAGE",      # Capturing images supported.
            0x02: "CAPTURE_VIDEO",      # Capturing videos supported.
            0x04: "CAPTURE_AUDIO",      # Capturing audio supported.
            0x08: "CAPTURE_PREVIEW",    # Capturing image previews supported.
            0x10: "CONFIG",             # Camera and Driver configuration supported.
            0x20: "TRIGGER_CAPTURE",    # Camera can trigger capture and wait for events.
        }

        def __init__(self, cameraAbilities):
            self._ca = cameraAbilities

        # Get the raw Swig object
        def Get(self):
            return self._ca

        # Name of camera model
        def GetModel(self):
            return self._ca.model

        # Driver quality
        def GetStatus(self):
            return (self._ca.status, self.GP_DRIVER_STATUS[self._ca.status])
        def GetStatusString(self):
            return self.GP_DRIVER_STATUS[self._ca.status]

        # Supported port types.
        def GetPort(self):
            return self._ca.port
        # Supported port types as List of (bit, string)
        def GetPortList(self):
            pr = self.GetPort()
            return [(b,s) for b,s in self.GP_PORT.items() if (pr & b > 0 or (b == 0 and pr == 0))]
        # Supported port types as CSV string
        def GetPortString(self):
            return ",".join([s for b,s in self.GetPortList()])

        # Supported serial port speeds (terminated with a value of 0).
        def GetSpeed(self):
            return self._ca.speed

        # Camera operation funcs.
        def GetOperations(self):
            return self._ca.operations
        # Camera operation funcs as List of (bit, string)
        def GetOperationsList(self):
            op = self.GetOperations()
            return [(b,s) for b,s in self.GP_OPERATION.items() if (op & b > 0 or (b == 0 and op == 0))]
        # Camera operation funcs as CSV string
        def GetOperationsString(self):
            return ",".join([s for b,s in self.GetOperationsList()])

        # Camera file operation funcs.
        def GetFileOperations(self):
            return self._ca.file_operations
        # Camera file operation funcs as List of (bit, string)
        def GetFileOperationsList(self):
            fo = self.GetFileOperations()
            return [(b,s) for b,s in self.GP_FILE_OPERATION.items() if (fo & b > 0 or (b == 0 and fo == 0))]
        # Camera file operation funcs as CSV string
        def GetFileOperationsString(self):
            return ",".join([s for b,s in self.GetFileOperationsList()])

        # Camera folder operation funcs.
        def GetFolderOperations(self):
            return self._ca.folder_operations
        # Camera folder operation funcs as List of (bit, string)
        def GetFolderOperationsList(self):
            fo = self.GetFolderOperations()
            return [(b,s) for b,s in self.GP_FOLDER_OPERATION.items() if (fo & b > 0 or (b == 0 and fo == 0))]
        # Camera folder operation funcs as CSV string
        def GetFolderOperationsString(self):
            return ",".join([s for b,s in self.GetFolderOperationsList()])

        # USB Vendor ID.
        def GetUsbVendor(self):
            return self._ca.usb_vendor
        # USB Product ID.
        def GetUsbProduct(self):
            return self._ca.usb_product
        # USB device class.
        def GetUsbClass(self):
            return self._ca.usb_class
        # USB device subclass.
        def GetUsbSubclass(self):
            return self._ca.usb_subclass
        # USB device protocol.
        def GetUsbProtocol(self):
            return self._ca.usb_protocol

        def GetDict(self):
            return {
                "model": self.GetModel(),
                "status": self.GetStatus(),
                "port": self.GetPortList(),
                "speed": self.GetSpeed(),
                "operations": self.GetOperationsList(),
                "file_operations": self.GetFileOperationsList(),
                "folder_operations": self.GetFolderOperationsList(),
                "usb_vendor": self.GetUsbVendor(),
                "usb_product": self.GetUsbProduct(),
                "usb_class": self.GetUsbClass(),
                "usb_subclass": self.GetUsbSubclass(),
                "usb_protocol": self.GetUsbProtocol(),
            }

        def __str__(self):
            return f"model: {self.GetModel()},\nstatus: {self.GetStatusString()},\nport: {self.GetPortString()},\nspeed: {self.GetSpeed()},\noperations: {self.GetOperationsString()},\nfile_operations: {self.GetFileOperationsString()},\nfolder_operations: {self.GetFolderOperationsString()},\nusb_vendor: {self.GetUsbVendor()},\nusb_product: {self.GetUsbProduct()},\nusb_class: {self.GetUsbClass()},\nusb_subclass: {self.GetUsbSubclass()},\nusb_protocol: {self.GetUsbProtocol()}"

    class Config():
        class SectionWidget():
            def __init__(self, camera_config, parent=None):
                if camera_config.get_readonly():
                    self.setDisabled(True)

                child_count = camera_config.count_children()

                if(child_count < 1):
                    return
                tabs = []
                for child in camera_config.get_children():
                    label = f"{child.get_label()} ({child.get_name()})"
                    print(label, end=" ")

                    child_type = child.get_type()
                    if(child_type == gp.GP_WIDGET_SECTION):
                        print("GP_WIDGET_SECTION")
                        tabs.append((Cam.Config.SectionWidget(child), label))
                    elif(child_type == gp.GP_WIDGET_TEXT):
                        print("GP_WIDGET_TEXT")
                    
                    elif(child_type == gp.GP_WIDGET_RANGE):
                        print("GP_WIDGET_RANGE")

                    elif(child_type == gp.GP_WIDGET_TOGGLE):
                        print("GP_WIDGET_TOGGLE")

                    elif(child_type == gp.GP_WIDGET_RADIO):
                        print("GP_WIDGET_RADIO")

                    elif(child_type == gp.GP_WIDGET_MENU):
                        print("GP_WIDGET_MENU")

                    elif(child_type == gp.GP_WIDGET_DATE):
                        print("GP_WIDGET_DATE")

                    else:
                        print("GP_WHAT_THE_FUCK_IS_THIS")
                        
        def __init__(self, cameraConfig):
            self._cc = cameraConfig

            print(self._cc)

            top_count = self._cc.count_children()
            print(top_count)
            top_widget = Cam.Config.SectionWidget(self._cc)


    GP_ERROR_CORRUPTED_DATA         = -102      # Data is corrupt. This error is reported by camera drivers if corrupted data has been received that can not be automatically handled. Normally, drivers will do everything possible to automatically recover from this error.
    GP_ERROR_FILE_EXISTS            = -103      # An operation failed because a file existed. This error is reported for example when the user tries to create a file that already exists.
    GP_ERROR_MODEL_NOT_FOUND        = -105      # The specified model could not be found. This error is reported when the user specified a model that does not seem to be supported by any driver.
    GP_ERROR_DIRECTORY_NOT_FOUND    = -107      # The specified directory could not be found. This error is reported when the user specified a directory that is non-existent.
    GP_ERROR_FILE_NOT_FOUND         = -108      # The specified file could not be found. This error is reported when the user wants to access a file that is non-existent.
    GP_ERROR_DIRECTORY_EXISTS       = -109      # The specified directory already exists. This error is reported for example when the user wants to create a directory that already exists.
    GP_ERROR_PATH_NOT_ABSOLUTE      = -111      # The specified path is not absolute. This error is reported when the user specifies paths that are not absolute, i.e. paths like "path/to/directory". As a rule of thumb, in gphoto2, there is nothing like relative paths.

    camera = None
    last_error = ""

    def __init__(self):
        pass
    
    def __enter__(self):
        self.Init()
        return self

    def __exit__(self, type, value, tb):
        self.Exit()

    def __bool__(self):
        return self.camera != None
        
    def __nonzero__(self):
        return self.__bool__()

    def Init(self):
        self.camera = gp.Camera()
        try:
            self.camera.init()
            return True
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)
        
        self.camera = None
        return False

    def Exit(self):
        try:
            if(self.__bool__()):
                self.camera.exit()
            return True
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)
        return False

    def GetSummary(self):
        try:
            return self.camera.get_summary()
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)
        return ""

    def GetAbilities(self):
        try:
            return Cam.Abilities(self.camera.get_abilities())
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)
        return ""

    def CaptureImage(self):
        try:
            print("Capturing Image..",end="")
            file_path = self.camera.capture(gp.GP_CAPTURE_IMAGE)
            print("done")

            target = f"/static/last_img{os.path.splitext(file_path.name)[-1]}"
            print(f"Saving Image to ({target})..",end="")
            camera_file = self.camera.file_get(file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL)
            print(f".",end="")
            camera_file.save(target)
            print("done!")

            return file_path
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)
        return False

    def GetConfig(self):
        try:
            self.config = Cam.Config(self.camera.get_config())
        except gp.GPhoto2Error as gpe:
            print(f"Exception: {str(gpe)}")
            self.last_error = str(gpe)


APP_STATE_0_CAMERA_NOT_CONNECTED =          (0, "Camera not connected.")
APP_STATE_1_CAMERA_CONNECTED_NOT_SYNCED =   (1, "Camera connected. Not Synchronized")
class App():
    app_settings = {}
    app_pages = []
    app_state = APP_STATE_0_CAMERA_NOT_CONNECTED

    class HomeHandler(tornado.web.RequestHandler):
        def get(self):
            self.render("index.html", app_pages=App.app_pages, active_page="Home", app_state=App.app_state)

    class CameraHandler(tornado.web.RequestHandler):
        def get(self):
            self.render("camera.html", app_pages=App.app_pages, active_page="Camera", app_state=App.app_state)

    class TimelapseHandler(tornado.web.RequestHandler):
        def get(self):
            self.render("timelapse.html", app_pages=App.app_pages, active_page="Timelapse", app_state=App.app_state)

    class CaptureHandler(tornado.web.RequestHandler):
        def get(self):
            self.put
    
    class ActionHandler(tornado.web.RequestHandler):
        def get(self, action):
            print(action)
            if(action == "btn_connect_camera"):
                App.action_cam_connect()
            elif(action == "btn_capture_image"):
                App.action_cam_capture_image()
            elif(action == "btn_get_config"):
                App.cam.GetConfig()

    def __init__(self):
        App.app_settings = {
            "port": 8888,
            "base_path": os.path.split(__file__)[0],
            "template_path": os.path.join(os.path.split(__file__)[0], "templates"),
            "static_path": os.path.join(os.path.split(__file__)[0],"static"),
        }
        App.app_pages = [
            {"name": "Home", "url": r"/", "handler": App.HomeHandler},
            {"name": "Camera", "url": r"/camera.html", "handler": App.CameraHandler},
            {"name": "Timelapse", "url": r"/timelapse.html", "handler": App.TimelapseHandler},
        ]

        print(App.app_settings)

        # Attempt to connect to a camera
        App.action_cam_connect()

        #asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
        self._app = self.make_app()
        self._app.listen(self.app_settings["port"])
        tornado.ioloop.IOLoop.current().start()

    def make_app(self):
        # Add the URL handlers
        app_handlers = []
        for page in self.app_pages:
            app_handlers.append((page["url"], page["handler"]))
        app_handlers.append((r'/static/(.*)', tornado.web.StaticFileHandler, {'path': "static/"}))
        app_handlers.append((r'/action/(.*)', App.ActionHandler))

        app_settings = {
            "template_path": os.path.join(self.app_settings["base_path"], self.app_settings["template_path"]),
            "static_path": os.path.join(self.app_settings["base_path"], self.app_settings["static_path"]),
        }

        return tornado.web.Application(app_handlers, **app_settings)

    def action_cam_connect():
        if(App.app_state == APP_STATE_0_CAMERA_NOT_CONNECTED):
            App.cam = Cam()
            if(App.cam.Init()):
                print("Camera initialized!")
                # Camera was initialized successfully
                App.app_state = APP_STATE_1_CAMERA_CONNECTED_NOT_SYNCED
                return True
            else:
                print("Camera failed to initialize!")
        else:
            print("Bad state!")
        return False

    def action_cam_capture_image():
        if(App.app_state == APP_STATE_1_CAMERA_CONNECTED_NOT_SYNCED):
            App.cam.CaptureImage()
        else:
            print("Bad state!")

if __name__ == "__main__":
    rpi = App()
    """
    try:
        with Cam() as cam:
            if(cam):
                print("\nSummary")
                print("=======")
                print(cam.GetSummary())

                print("\nAbilities")
                print("=======")
                print(cam.GetAbilities())

                img_path = cam.CaptureImage()
                if(img_path):
                    print(type(img_path))
                    print(f"{img_path.folder}/{img_path.name}")
            else:
                print(cam.last_error)
    except gp.GPhoto2Error as gpe:
        print(f"Unhandled GPhoto2 Exception: {str(gpe)}")
    except Exception as e:
        print(f"Unhandled exception: {str(e)}")
        raise (e)
    """